package com.precorconnect.registrationlogservice;

import org.checkerframework.checker.nullness.qual.NonNull;

interface AddRegistrationLogFeature {

	PartnerSaleRegistrationId execute(
            @NonNull AddPartnerSaleRegistrationLog request
           
    );

}
