package com.precorconnect.registrationlogservice;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface ListDealerRepFirstAndLastNameFeature {
	
	Collection<RegistrationDealerRepView> execute(
            @NonNull List<PartnerSaleRegistrationId> registrationIds
           
    );

}
