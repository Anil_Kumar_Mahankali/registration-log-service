package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

@Singleton
public class UpdateRegistrationLogFeatureImpl 
					implements UpdateRegistrationLogFeature {
	
	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateRegistrationLogFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
            @NonNull List<UpdatePartnerSaleRegistrationLog> request
    ) {
        
            databaseAdapter
                        .updateRegistrationLog(
                                request
                        );

    }

}
