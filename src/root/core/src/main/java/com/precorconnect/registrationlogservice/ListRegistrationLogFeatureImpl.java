package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Singleton;
import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;


@Singleton	
public class ListRegistrationLogFeatureImpl 
				implements ListRegistrationLogFeature{

	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListRegistrationLogFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<RegistrationLogView> listRegistrationLogWithId(
			@NonNull AccountId accountId
			) throws AuthenticationException {

		return
				databaseAdapter
					.listSpiffLogsWithId(accountId);
	}
}
