package com.precorconnect.registrationlogservice;

import java.time.Instant;

public interface SubmittedDate {
	
	Instant getValue();

}
