package com.precorconnect.registrationlogservice;

public interface UpdatePartnerSaleRegistrationLog {
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();
	
	SpiffStatus getSpiffStatus();

}
