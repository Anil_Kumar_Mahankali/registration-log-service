package com.precorconnect.registrationlogservice;

public interface PartnerSaleRegistrationId {
	
	Long getValue();

}
