package com.precorconnect.registrationlogservice;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;

public interface RegistrationDealerRepView {
	
	FirstName getFirstName();
	
	LastName getLastName();
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();

}
