package com.precorconnect.registrationlogservice;

import java.util.Optional;

import com.precorconnect.AccountId;

public interface RegistrationLogView {
	
	RegistrationId getRegistrationId();
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();
	
    AccountId getAccountId();
	
	SellDate getSellDate();
	
	Optional<InstallDate> getInstallDate();
	
	SubmittedDate getSubmittedDate();
	
	FacilityName getFacilityName();
	
	ExtendedWarrantyStatus getExtendedWarrantyStatus();
	
	SpiffStatus getSpiffStatus();
	
	
}
