package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;
import com.precorconnect.EmailAddress;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public class AddPartnerSaleRegistrationLogImpl 
				implements AddPartnerSaleRegistrationLog {
	
	private PartnerSaleRegistrationId partnerSaleRegistrationId;
	private AccountId accountId;
	private AccountName accountName;
	private SellDate sellDate;
	private InstallDate installDate;
	private SubmittedDate submittedDate;
	private ExtendedWarrantyStatus extendedWarrantyStatus;
	private SpiffStatus spiffStatus;
	private FirstName firstName;
	private LastName lastName;
	private EmailAddress emailAddress;
	private UserId userId;
	private AccountName submittedByName;
	
	
	 public AddPartnerSaleRegistrationLogImpl(
	            @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
	            @NonNull AccountId accountId,
	            @NonNull AccountName accountName,
	            @NonNull SellDate sellDate,
	            @Nullable InstallDate installDate,
	            @NonNull SubmittedDate submittedDate,
	            @NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
	            @NonNull SpiffStatus spiffStatus,
	            @Nullable FirstName firstName,
	            @Nullable LastName lastName,
	            @Nullable EmailAddress emailAddress,
	            @Nullable UserId userId,
	            @Nullable AccountName submittedByName
	    ) {

	        this.partnerSaleRegistrationId =
	                guardThat(
	                        "partnerSaleRegistrationId",
	                        partnerSaleRegistrationId
	                )
	                        .isNotNull()
	                        .thenGetValue();

	        this.accountId =
	                guardThat(
	                        "accountId",
	                        accountId
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.accountName =
	                guardThat(
	                        "accountName",
	                        accountName
	                )
	                        .isNotNull()
	                        .thenGetValue();

	        this.sellDate =
	        		guardThat(
	        				"sellDate",
	        				sellDate
	        				)
	        				.isNotNull()
	        				.thenGetValue();
	        
	        this.installDate = installDate;
	               
	        this.submittedDate =
	                guardThat(
	                        "submittedDate",
	                        submittedDate
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.extendedWarrantyStatus =
	                guardThat(
	                        "extendedWarrantyStatus",
	                        extendedWarrantyStatus
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.spiffStatus =
	                guardThat(
	                        "spiffStatus",
	                        spiffStatus
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.firstName = firstName;
	        
	        this.lastName = lastName;
	        
	        this.emailAddress = emailAddress;
	        
	        this.userId = userId;
	        
	        this.submittedByName = submittedByName;

	    }


	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public AccountId getAccountId() {
		return accountId;
	}

	@Override
	public AccountName getAccountName() {
		return accountName;
	}

	@Override
	public Optional<InstallDate> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	@Override
	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public SubmittedDate getSubmittedDate() {
		return submittedDate;
	}

	@Override
	public ExtendedWarrantyStatus getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}


	@Override
	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}
	
	@Override
	public FirstName getFirstName() {
		return firstName;
	}

	@Override
	public LastName getLastName() {
		return lastName;
	}

	@Override
	public EmailAddress getEmailAddress() {
		return emailAddress;
	}

	@Override
	public UserId getUserId() {
		return userId;
	}

	@Override
	public AccountName getSubmittedByName() {
		return submittedByName;
	}

}
