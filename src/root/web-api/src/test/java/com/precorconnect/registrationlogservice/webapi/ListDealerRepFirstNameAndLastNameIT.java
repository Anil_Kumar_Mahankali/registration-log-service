package com.precorconnect.registrationlogservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/listDealerRepFirstAndLastName.feature"},
		glue={"com.precorconnect.registrationlogservice.webapi.listdealernames"}
		)
public class ListDealerRepFirstNameAndLastNameIT {

}
