package com.precorconnect.registrationlogservice.webapi.listdealernames;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.registrationlogservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.registrationlogservice.webapi.Config;
import com.precorconnect.registrationlogservice.webapi.ConfigFactory;
import com.precorconnect.registrationlogservice.webapi.Dummy;
import com.precorconnect.registrationlogservice.webapi.Factory;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs 
			extends AbstractSpringIntegrationTest {
		
		 private final Config config =
		            new ConfigFactory().construct();

		    private final Dummy dummy = new Dummy();

		    private final Factory factory =
		            new Factory(
		                    dummy,
		                    new IdentityServiceIntegrationTestSdkImpl(
		                            config.getIdentityServiceJwtSigningKey()
		                    )
		            );

		    private OAuth2AccessToken accessToken;
		    
		    
		    private List<Long> registationIds = new ArrayList<Long>();
		    
		    
		    private Response response;

		    
		    @Before
		    public void beforeAll() {

		        RestAssured.port = getPort();

		    }
		    
		    
		    @Given("^list of Partner Rep Ids consists of:$")
		    public void list_of_Partner_Rep_Ids_consists_of(DataTable arg1) throws Throwable {
		    	// No op list of partner rep ids are from dummy
		    }

		    @Given("^I provide an accessToken identifying me as <identity>$")
		    public void i_provide_an_accessToken_identifying_me_as_identity() throws Throwable {
		        
		    	accessToken = new OAuth2AccessTokenImpl(
						factory
							.constructValidPartnerRepOAuth2AccessToken()
							.getValue()
						);
		    }

		    @Given("^provide a valid Partner Rep Ids$")
		    public void provide_a_valid_Partner_Rep_Ids() throws Throwable {
		    	
		    	
		    	registationIds.add(1234l);
		    	registationIds.add(1228l);
		    	
		    }

		    @When("^I GET to /partnerRegistrationIds$")
		    public void i_GET_to_partnerRegistrationIds() throws Throwable {
		    	
		    	StringJoiner joiner=new StringJoiner(",");

			   	  for(Long registationId : registationIds){
			   		joiner.add(""+registationId);
			   	  }
			   	  
		    	response =
		    			given()
		                .contentType(ContentType.JSON)
		                .header(
		                        "Authorization",
		                        String.format(
		                                "Bearer %s",
		                                accessToken
		                                        .getValue()
		                        )
		                )
		                .get("/registration-log/partnerRegistrationIds/"+joiner);
		    }

		    @Then("^list of dealer First Name and Last Name which are associated to Partner Rep Ids from registration-log table$")
		    public void list_of_dealer_First_Name_and_Last_Name_which_are_associated_to_Partner_Rep_Ids_from_registration_log_table() throws Throwable {
		    	
		    	response
    				.then()
    				.assertThat()
    				.statusCode(200);
		    }


}
