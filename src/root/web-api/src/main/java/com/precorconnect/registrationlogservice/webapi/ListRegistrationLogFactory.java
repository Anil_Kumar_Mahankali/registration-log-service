package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.RegistrationLogView;


public interface ListRegistrationLogFactory {

	RegistrationLogSynopsisView construct(
			@NonNull RegistrationLogView registrationLogView
			);
}
