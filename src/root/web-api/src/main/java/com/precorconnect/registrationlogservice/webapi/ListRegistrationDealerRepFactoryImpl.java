package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.registrationlogservice.RegistrationDealerRepView;

@Component
public class ListRegistrationDealerRepFactoryImpl 
					implements ListRegistrationDealerRepFactory {

	@Override
	public RegistrationDealerRepSynopsisView construct(
			@NonNull RegistrationDealerRepView registrationDealerRepView) {
		
		String firstName = registrationDealerRepView.getFirstName()==null?null:registrationDealerRepView.getFirstName().getValue();
		
		String lastName = registrationDealerRepView.getLastName()==null?null:registrationDealerRepView.getLastName().getValue();
		
		Long registrationId = registrationDealerRepView.getPartnerSaleRegistrationId().getValue();
		
		return new RegistrationDealerRepSynopsisView(
				registrationId,
				firstName, 
				lastName
			);
	}

}
