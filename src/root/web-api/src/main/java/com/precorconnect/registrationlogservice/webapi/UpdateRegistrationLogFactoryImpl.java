package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.UpdateEWSatusRegistrationLog;
import com.precorconnect.registrationlogservice.UpdateEWSatusRegistrationLogImpl;

@Component
public class UpdateRegistrationLogFactoryImpl implements UpdateRegistrationLogFactory {

	@Override
	public UpdateEWSatusRegistrationLog construct(
			com.precorconnect.registrationlogservice.webapi.@NonNull UpdateEWSatusRegistrationLog updateEWSatusRegistrationLog) {

		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(updateEWSatusRegistrationLog.getPartnerSaleRegistrationId());
		
		ExtendedWarrantyStatus extendedWarrantyStatus = new ExtendedWarrantyStatusImpl(updateEWSatusRegistrationLog.getEwStatus());
		
		return new UpdateEWSatusRegistrationLogImpl(
				partnerSaleRegistrationId,
				extendedWarrantyStatus
				);
	}

	

}
