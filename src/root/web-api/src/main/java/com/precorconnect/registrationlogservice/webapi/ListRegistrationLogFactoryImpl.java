package com.precorconnect.registrationlogservice.webapi;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.registrationlogservice.RegistrationLogView;

@Component
public class ListRegistrationLogFactoryImpl
		implements ListRegistrationLogFactory {

	@Override
	public RegistrationLogSynopsisView construct(
			@NonNull RegistrationLogView registrationLogView
			) {
		
		Long registrationId = 
				registrationLogView
						.getRegistrationId()
						.getValue();
		
		Long partnerSaleRegistrationId = 
				registrationLogView
						.getPartnerSaleRegistrationId()
						.getValue();
		
		String accountId = 
				registrationLogView
						.getAccountId()
						.getValue();
		
		String sellDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(registrationLogView
						 .getSellDate().getValue())
				 	);
		
		String installDate =
				registrationLogView
					.getInstallDate()
					.isPresent()?
						new SimpleDateFormat("MM/dd/yyyy").format(
								Date.from(registrationLogView
										.getInstallDate().get().getValue())
								)
						:null;
		
		String submittedDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(registrationLogView
						 .getSubmittedDate().getValue())
				 	);
		
		String facilityName = 
				registrationLogView
						.getFacilityName()
						.getValue();
		
		String extendedWarrantyStatus = 
				registrationLogView
						.getExtendedWarrantyStatus()
						.getValue();
		
		String spiffStatus = 
				registrationLogView
						.getSpiffStatus()
						.getValue();
		
		
		return 
				new RegistrationLogSynopsisView(
						registrationId, 
						partnerSaleRegistrationId, 
						accountId, 
						sellDate, 
						installDate, 
						submittedDate, 
						facilityName, 
						extendedWarrantyStatus, 
						spiffStatus
						);
				
	}

}
