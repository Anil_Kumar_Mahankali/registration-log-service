Feature: Add Registration Log 
	adds a registration log
	
	Background:
    Given an addRegistrationLog consists of:
      | attribute    				 | validation | type   |
      | partnerSaleRegistrationId    | required   | Long |
      | partnerAccountId             | required   | string |
      | accountName 				 | required   | string |
      | sellDate    			     | required   | string |
      | installDate 				 | required   | string |
      | submittedDate    			 | required   | string |
      
   Scenario: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid addRegistrationLog
    When I POST to /addRegistrationLog
    Then a registration log is added to the registration-log table with the provided attributes
    And its id is returned