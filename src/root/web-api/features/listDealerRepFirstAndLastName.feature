Feature:  List Dealer Rep First Name and Last Name 
	Retrieve Dealer Rep First Name and Last Name based on given Partner Rep Id
	
	Background:
    Given list of Partner Rep Ids consists of:
      | attribute    					  | validation | type   |
      | Partner Rep Id                    | required   | string |
      
   Scenario: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid Partner Rep Ids
    When I GET to /partnerRegistrationIds
    Then list of dealer First Name and Last Name which are associated to Partner Rep Ids from registration-log table