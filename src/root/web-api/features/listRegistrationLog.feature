Feature:  List Registration Log 
	Retrieve registration log using Account Id
	
	Background:
    Given an addRegistrationLog consists of:
      | attribute    				 | validation | type   |
      | AccountId                    | required   | string |
      
   Scenario: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid AccountId
    When I GET to /AccountId
    Then list of registration log is details which are associated to Account Id fetch from registration-log table