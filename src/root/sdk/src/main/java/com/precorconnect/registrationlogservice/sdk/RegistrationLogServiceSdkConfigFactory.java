package com.precorconnect.registrationlogservice.sdk;

public interface RegistrationLogServiceSdkConfigFactory {
	
	RegistrationLogServiceSdkConfig construct();

}
