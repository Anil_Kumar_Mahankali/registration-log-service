package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.RegistrationDealerRepView;

public interface RegistrationDealerRepFactory {
	
	RegistrationDealerRepView construct(
			@NonNull RegistrationLog registrationLog
		);

}
