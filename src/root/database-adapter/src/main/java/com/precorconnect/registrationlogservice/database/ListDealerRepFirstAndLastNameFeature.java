package com.precorconnect.registrationlogservice.database;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.RegistrationDealerRepView;

public interface ListDealerRepFirstAndLastNameFeature {
	
	Collection<RegistrationDealerRepView> execute(
            @NonNull List<PartnerSaleRegistrationId> registrationIds
           
    );

}
