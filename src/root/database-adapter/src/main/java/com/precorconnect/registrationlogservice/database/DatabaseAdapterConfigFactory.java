package com.precorconnect.registrationlogservice.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();
}
