package com.precorconnect.registrationlogservice.identityservice;

import com.precorconnect.AccessContext;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AuthenticationException;
import org.checkerframework.checker.nullness.qual.NonNull;

interface GetAccessContextFeature {

    AccessContext execute(
    		@NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException;
            
    

}
