package com.precorconnect.registrationlogservice.api;

public interface RegistrationLogServiceConfigFactory {

	RegistrationLogServiceConfig construct();
}
